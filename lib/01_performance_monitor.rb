def measure(repeat = 1)
  start_time = Time.now
  repeat.times do
    yield
  end
  time_elapsed = Time.now - start_time
  average = time_elapsed/repeat
end
