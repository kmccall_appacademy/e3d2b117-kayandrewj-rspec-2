def eval_block(*values, &block)
  if block == nil
    raise "NO BLOCK GIVEN!"
  else
    block.call(*values)
  end
end
