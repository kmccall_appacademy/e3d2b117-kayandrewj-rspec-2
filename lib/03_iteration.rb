def factors(num)
  factors = []
  (1..num).each do |x|
    if num % x == 0
      factors << x
    end
  end
  factors
end

class Array

  def bubble_sort!(&prc)
    prc ||= Proc.new { |num1, num2| num1 <=> num2 }
    sorted = false
    until sorted
      sorted = true

      each_index do |i|
        next if i + 1 == self.length
        j = i + 1
        if prc.call(self[i], self[j]) == 1
          sorted = false
          self[i], self[j] = self[j], self[i]
        end
      end
    end

    self
  end

  def bubble_sort(&prc)
    self.dup.bubble_sort!(&prc)
  end

end

def substrings(string)
  subs = []

  string.length.times do |substr_begin| #run lines 54 and 55 for as many characters are in the string
    ((substr_begin)...(string.length)).each do |substr_end| #run the next bit as many times as the characters between the start of the substring and the end of the string
      substr = string[substr_begin..substr_end] #extract a single substring

      subs << substr unless subs.include?(substr) #push strings and eliminate repeating substrings
    end
  end
  subs
end

def subwords(word, dictionary)
  substrings(word).select { |x| dictionary.include?(x)}
end

def doubler(array)
  array.map do |num|
    num*2
  end
end

class Array
  def my_each(&prc)
    i = 0
    while i < self.length
      prc.call(self[i])
      i+=1
    end
    self
  end
end

# ### My Enumerable Methods
# * Implement new `Array` methods `my_map` and `my_select`. Do
#   it by monkey-patching the `Array` class. Don't use any of the
#   original versions when writing these. Use your `my_each` method to
#   define the others. Remember that `each`/`map`/`select` do not modify
#   the original array.
# * Implement a `my_inject` method. Your version shouldn't take an
#   optional starting argument; just use the first element. Ruby's
#   `inject` is fancy (you can write `[1, 2, 3].inject(:+)` to shorten
#   up `[1, 2, 3].inject { |sum, num| sum + num }`), but do the block
#   (and not the symbol) version. Again, use your `my_each` to define
#   `my_inject`. Again, do not modify the original array.

class Array
  def my_map(&prc)
    final_array = [] #stores values got from line 88
    my_each { |piece|  final_array << prc.call(piece)} #iterates through data, calling passed in block on each piece
    final_array #returns 'mapped' pieces as an array.
  end

  def my_select(&prc)
    final_array = []
    my_each { |piece| final_array << piece if prc.call(piece) }
    final_array
  end

  def my_inject(&blk)
    num = self.first
    self.drop(1).my_each { |piece| num = blk.call(num, piece) }
    num
  end
end

# ### Concatenate
# Create a method that takes in an `Array` of `String`s and uses `inject`
# to return the concatenation of the strings.
#
# ```ruby
# concatenate(["Yay ", "for ", "strings!"])
# # => "Yay for strings!"
# ```

def concatenate(strings)
  strings.inject { |a, b| a + b}
  strings.join("")
end
