def reverser
  words = yield.split
  words.map! { |word| word.reverse}
  words.join(" ")
end

def adder(num=1)
  yield + num
end

def repeater(repeat = 1)
  repeat.times do
    yield
  end
end
